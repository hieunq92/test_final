<?php
function phone_number ($value){
	if(preg_match('/^(84|0)(85|81|82|83|91|94|88|84|90|89|93|70|79|77|76|78|96|97|98|32|33|34|35|36|37|38|39|86|99|59|92|56|58)\d{7}$/', $value, $matches)){
		return true;
	}
	return false;
};

function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}