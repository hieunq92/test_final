<?php

use Phroute\Phroute\Dispatcher;
use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\RouteParser;
require_once "lib/config.php";
require_once "lib/sql.php";
require_once "lib/function.php";
require_once 'vendor/autoload.php';


$router = new RouteCollector(new RouteParser());
$header = getallheaders();
$api_token = isset($header['api_token']) ? $header['api_token'] : '';

$router->filter('auth', function() use ($db,$api_token){
	if(!empty($api_token)){
		$query = "SELECT api_token FROM customers WHERE api_token = '$api_token'"; 
		if($db->num_rows($query) == 0 ){
			return json_encode(['error_code'=>4,'message'=>'Mã Token Không tồn tại']);
		}
	}else {
		return json_encode(['error_code'=>3,'message'=>'Vui lòng nhập mã token']);
	}
});

$router->post('api/login', function() use ($db){
	$email	= isset($_POST['email'])	? $db->filter(trim(htmlspecialchars($_POST['email']))) : '';
	$password = isset($_POST['password'])	? $db->filter(trim(htmlspecialchars($_POST['password']))) : '';
	if(!empty($email) && !empty($password)){
		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$password = md5(md5($password));
			$query = "SELECT id,email FROM customers WHERE email = '$email' and password = '$password' and status = 1";
			if($db->num_rows($query)>0 ){
				list($id) = $db->get_row( $query );
				$token = generateRandomString(256);
				$db->update( 'customers', ['api_token'=>$token],array('id' => $id) );
				return json_encode(['error_code'=>0,'message'=>'Đăng nhập thành công','token'=>$token]);
			}else {
				return json_encode(['error_code'=>1,'message'=>'Tên tài khoản hoặc mật khẩu không đúng']);
			}
		} else{
			return json_encode(['error_code'=>2,'message'=>'Địa chỉ email không hợp lệ','field'=>'email']);
		}
	}else {
		if(empty($email)){
			return json_encode(['error_code'=>2,'message'=>'Vui lòng nhập email','field'=>'email']);
		}
		if(empty($password)){
			return json_encode(['error_code'=>2,'message'=>'Vui lòng nhập password','field'=>'password']);
		}
	}
	return json_encode(['error_code'=>0,'message'=>'success','data'=>$results]);
});

$router->group(['before' => 'auth','prefix'=>'api/customer'], function ($router) use ($db,$api_token){
	
	$router->get('/', function () use ($db){
		$results = $db->get_results("SELECT email,full_name,address,phone FROM customers order by id desc");
		return json_encode(['error_code'=>0,'message'=>'success','data'=>$results]);
	});
	
	$router->post('update',function ()use ($db,$api_token){
		$name	= isset($_POST['name'])	? $db->filter(trim($_POST['name'])) : '';
		$address	= isset($_POST['address'])	? $db->filter(trim($_POST['address'])) : '';
		$phone	= isset($_POST['phone'])	? $db->filter(trim($_POST['phone'])) : '';
		$update['full_name'] = $name;
		if(!empty($phone)){
			if(phone_number($phone) == true){
				$update['phone'] = $phone;
			}else {
				return json_encode(['error_code'=>2,'message'=>'Định dạng số điện thoại không đúng','field'=>'phone']);
			}
		}
		$response = $db->update( 'customers', $update,array('api_token' => $api_token) );
		return json_encode(['error_code'=>0,'message'=>'Cập nhật dữ liệu thành công']);
	});
	
	$router->get('logout',function () use ($db,$api_token){
		$response = $db->update( 'customers', ['api_token'=>'NULL'],array('api_token' => $api_token) );
		return json_encode(['error_code'=>0,'message'=>'Thoát thành công']);
	});
});
try{
	$dispatcher = new Dispatcher($router->getData());
	$response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
	echo $response;	
}catch (Exception $ex){
	echo json_encode(['error_code'=>500,'message'=>'Lỗi hệ thống']);
}
